package br.com.alura.ecommerce;

import java.math.BigDecimal;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

public class NewOrderMain {

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		try (KafkaDispatcher<Order> orderDispacther = new KafkaDispatcher<Order>()) { // Tento executar esse código e se acontecer qualquer exception nesse bloco ele fecha o KafkaDispatcher 
			try (KafkaDispatcher<String> emailDispacther = new KafkaDispatcher<String>()) {
				String userId = UUID.randomUUID().toString(); // Gero um id aleatório do usuario
				String orderId = UUID.randomUUID().toString(); // Gero um id aleatório da orderm
				BigDecimal amount = new BigDecimal(Math.random() * 5000 + 1);

				Order order = new Order(userId, orderId, amount);

				orderDispacther.send("ECOMMERCE_NEW_ORDER", userId, order);

				String email = "Thank you for your order! We are processing your order!";
				emailDispacther.send("ECOMMERCE_SEND_EMAIL", userId, email);
			}
		}
	}

}
